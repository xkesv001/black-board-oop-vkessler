package cz.enehano.skoleni.blackboard;

import cz.enehano.skoleni.blackboard.service.BlackBoardService;

public class BlackBoardMain {

    public static void main(String[] args) {
        BlackBoardService boardService = new BlackBoardService();
        boardService.printAllShapes();
    }
}
