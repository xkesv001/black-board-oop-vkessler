package cz.enehano.skoleni.blackboard.service;

public class BlackBoardService {

    /**
     * in this class will be all operation with board
     *  - add new shape to board (new shape will be created in ShapeService)
     *  - delete share from board
     *  - list all shapes and another point from requirements.
     */

    private ShapeService shapeService = new ShapeService();

    public void printAllShapes() {
        System.out.print("This is all shapes on you board....");
    }
}
