package cz.enehano.skoleni.blackboard.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Square extends Shape {

    private Integer side;

    public Integer getArea() {
        return 4 * side;
    }

    public Integer getVolume() {
        return side * side;
    }
}
