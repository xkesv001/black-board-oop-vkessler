package cz.enehano.skoleni.blackboard.dto;

public abstract class Shape {

    /**
     * this is a parent of all dto in our application.
     */

    public abstract Integer getArea();

    public abstract Integer getVolume();
}
