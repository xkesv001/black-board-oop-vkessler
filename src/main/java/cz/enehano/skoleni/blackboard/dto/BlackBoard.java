package cz.enehano.skoleni.blackboard.dto;

import lombok.Data;

import java.util.Collection;

@Data
public class BlackBoard {

    private Collection<Shape> shapesOnBoard;
    private Shape boardShape;
}
